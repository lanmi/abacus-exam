# README #

This is simple application that displays numbers that I am designing for my kids.

## Credits

* Semaphore image designed by Freepik from www.flaticon.com
* English audio:http://soundbible.com/2008-0-9-Male-Vocalized.html
* Success sound: http://soundbible.com/1964-Small-Crowd-Applause.html
* Fail sound: http://soundbible.com/1830-Sad-Trombone.html
* Sempahore sound effects: https://www.zapsplat.com