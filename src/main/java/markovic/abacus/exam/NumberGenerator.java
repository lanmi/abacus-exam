package markovic.abacus.exam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class NumberGenerator {

    private static Random rand = new Random();
    private static Collection<Integer> POSITIVE_DIGITS =
            Collections.unmodifiableCollection(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));
    private static Collection<Integer> NEGATIVE_DIGITS =
            Collections.unmodifiableCollection(Arrays.asList(-1, -2, -3, -4, -5, -6, -7, -8, -9));

    private NumberGenerator() {

    }

    public static List<Integer> generateNumbers(int length, int downBound, int upperBound) {
        if (upperBound < downBound) {
            throw new IllegalArgumentException(
                    "Down boundary (" + downBound + ") is greater than upper boundary (" + upperBound + ")");
        }
        List<Integer> numbers = new ArrayList<>();

        for (int i = 0; i < length; i++) {
            numbers.add(randomIntFromRange(downBound, upperBound));
        }

        return numbers;
    }

    private static int randomIntFromRange(int downBound, int upperBound) {
        int range = upperBound - downBound;
        return rand.nextInt(range) + downBound;
    }

    public static List<Integer> generateOneDigitUnsignedNumbers(int length) {
        return generateNumbers(length, 1, 9);
    }

    public static List<Integer> generateOneDigitSignedNumbers(int length) {
        return rand.ints(-9, 9)
                .filter(num -> num != 0)
                .limit(length)
                .boxed()
                .collect(Collectors.toList());
    }

    public static List<Integer> generateOneDigitSignedNoAlgorithmNumberList(int length) {
        return generateListFromAlgorithmGenerator(length, NumberGenerator::generateNextSignedNoAlgorithmNumber);
    }

    public static List<Integer> generateTwoDigitSignedNoAlgorithmNumber(int length) {
        return generateListFromAlgorithmGenerator(length, NumberGenerator::generateNextTwoDigitSignedNoAlgorithmNumber);
    }

    private static int generateNextTwoDigitSignedNoAlgorithmNumber(int previousSum) {
        return generateNextSignedNoAlgorithmNumber(previousSum, 2);
    }

    private static int generateNextSignedNoAlgorithmNumber(int previousSum, int digitsCount) {
        int sign = decideSign(previousSum, digitsCount);
        Collection<Integer> exclude;
        if (sign < 0) {
            exclude = POSITIVE_DIGITS;
        } else {
            exclude = NEGATIVE_DIGITS;
        }

        int next = 0;
        do {
            for (int i = 0; i < digitsCount; i++) {
                int previousOnDigitSum = getDigit(previousSum, i);
                next += Math.pow(10, i) * generateNextSignedNoAlgorithmNumber(previousOnDigitSum, exclude);
            }
        } while (next == 0);
        return next;
    }

    static int getDigit(int number, int digitPosition) {
        String numberStr = "00000000" + number;
        int length = numberStr.length();

        String result = numberStr.substring(length - 1 - digitPosition, length - digitPosition);
        return Integer.parseInt(result);
    }

    private static int decideSign(int previousSum, int digitsCount) {
        Collection<Integer> exclude;
        if (previousSum == 0) {
            return 1;
        }
        if (Math.pow(10, digitsCount) - 1 == previousSum) {
            return -1;
        }
        boolean isPositive = Math.random() < 0.5;
        if (isPositive) {
            return 1;
        } else {
            return -1;
        }

    }

    private static int generateNextSignedNoAlgorithmNumber(int previousSum) {
        Collection<Integer> exclude = Collections.singletonList(0);
        return generateNextSignedNoAlgorithmNumber(previousSum, exclude);
    }

    private static int generateNextSignedNoAlgorithmNumber(int previousSum, Collection<Integer> exclude) {
        switch (previousSum) {
        case 0:
            return pickOne(of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9), exclude);
        case 1:
            return pickOne(of(0, 1, 2, 3, 5, 6, 7, 8, -1), exclude);
        case 2:
            return pickOne(of(0, 1, 2, 5, 6, 7, -1, -2), exclude);
        case 3:
            return pickOne(of(0, 1, 5, 6, -1, -2, -3), exclude);
        case 4:
            return pickOne(of(0, 5, -1, -2, -3, -4), exclude);
        case 5:
            return pickOne(of(0, 1, 2, 3, 4, -5), exclude);
        case 6:
            return pickOne(of(0, 1, 2, 3, -1, -5, -6), exclude);
        case 7:
            return pickOne(of(0, 1, 2, -1, -2, -5, -6, -7), exclude);
        case 8:
            return pickOne(of(0, 1, -1, -2, -3, -5, -6, -7, -8), exclude);
        case 9:
            return pickOne(of(0, -1, -2, -3, -4, -5, -6, -7, -8, -9), exclude);
        default:
            throw new IllegalArgumentException("Can't calculate next number from: " + previousSum);
        }
    }

    public static List<Integer> generateSigned5sAlgorithmNumberList(int length) {
        return generateListFromAlgorithmGenerator(length, NumberGenerator::generateNextSigned5sAlgorithmNumber);
    }

    private static int generateNextSigned5sAlgorithmNumber(int previousSum) {
        return pickOne(zerolessRange(1 - previousSum, 9 - previousSum));
    }

    private static int[] zerolessRange(int from, int to) {
        return IntStream.range(from, to + 1)
                .filter(i -> i != 0)
                .toArray();
    }

    public static List<Integer> generateSignedAllAlgorithmsNumberList(int length) {
        return generateListFromAlgorithmGenerator(length, NumberGenerator::generateNextSignedAllAlgorithmNumber);
    }

    private static int generateNextSignedAllAlgorithmNumber(int previousSum) {
        if (previousSum < 0) {
            throw new IllegalArgumentException(
                    "Previous sum must be not less than zero. Currently it is: " + previousSum);
        }
        return pickOne(zerolessRange(-previousSum, 9));
    }

    private static List<Integer> generateListFromAlgorithmGenerator(int listLength, Function<Integer, Integer> generator) {
        List<Integer> numbers = new ArrayList<>();
        int sum = 0;
        for (int i = 0; i < listLength; i++) {
            int next = generator.apply(sum);
            numbers.add(next);
            sum += next;
        }
        return numbers;
    }

    private static int pickOne(int... of) {
        int size = of.length;
        return of[rand.nextInt(size)];
    }

    private static int pickOne(Collection<Integer> of, Collection<Integer> exclude) {
        return pickOne(of.stream().mapToInt(Integer::intValue).filter(element -> !exclude.contains(element)).toArray());
    }

    private static int pickOne(Collection<Integer> of) {
        return pickOne(of, Collections.emptySet());
    }

    private static Collection<Integer> of(int... of) {
        return Arrays.stream(of)
                .boxed()
                .collect(Collectors.toList());
    }
}

