package markovic.abacus.exam.utilites;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Util {

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }
}
