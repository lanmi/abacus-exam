package markovic.abacus.exam;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import javax.swing.SwingUtilities;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import markovic.abacus.exam.audio.AudioPlayer;
import markovic.abacus.exam.controller.NumberDisplayController;
import markovic.abacus.exam.controller.ResultEnterController;
import markovic.abacus.exam.controller.SemaphoreController;
import static markovic.abacus.exam.utilites.Util.sleep;

@Slf4j
public class ExamExecutor {

    private static final ExamExecutor instance = new ExamExecutor();

    private int delayBetweenNumbers = 1000;

    private final Executor executor = Executors.newSingleThreadExecutor();
    private final AudioPlayer audioPlayer = new AudioPlayer();
    private List<Integer> numbers;

    private volatile boolean isRunning = false;

    private ExamExecutor() {
    }

    public static ExamExecutor getInstance() {
        return instance;
    }

    public void start(int delayBetweenNumbers, List<Integer> numbers) {
        if (isRunning) {
            log.warn("Trying to run a new exam while one is already in progress.");
            return;
        }
        isRunning = true;
        this.delayBetweenNumbers = delayBetweenNumbers;
        this.numbers = numbers;
        start();
    }

    private void start() {
        SwingUtilities.invokeLater(this::startInternal);
    }

    private void startInternal() {
        executor.execute(this::startInThread);
    }

    private void startInThread() {
        log.info("Starting exam for delay {} and numbers {}", delayBetweenNumbers, numbers);
        displaySemaphore();
        displayNumbers(numbers);
        int result = numbers.stream().reduce(0, Integer::sum);
        this.displayResultEnterDialog(result);
        isRunning = false;
    }

    private void displaySemaphore() {
        try {
            final CountDownLatch animationFinished = new CountDownLatch(1);
            SemaphoreController controller = loadFxWindow("fxml/Semaphore.fxml");
            controller.setCallback(animationFinished::countDown);
            controller.play();
            animationFinished.await();
        } catch (InterruptedException e) {
            log.error("", e);
        }
    }

    private <T> T loadFxWindow(String fxml) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(fxml));
            Parent parent = loader.load();
            T controller = loader.getController();
            Platform.runLater(() -> {
                Scene scene = new Scene(parent);
                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setScene(scene);
                stage.show();
            });
            return controller;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private void displayResultEnterDialog(int result) {
        ResultEnterController controller = loadFxWindow("fxml/ResultEnterDialog.fxml");
        controller.setExpectedResult(result);
    }

    private void displayNumbers(List<Integer> numbers) {
        NumberDisplayController controller = loadFxWindow("fxml/NumberDisplay.fxml");
        for (int number : numbers) {
            controller.setNumber(number);
            speakNumber(number);
            doPauseBetween();
        }
        controller.close();
    }

    private void speakNumber(int number) {
        audioPlayer.playNumberNonBlocking(number);
    }

    private void doPauseBetween() {
        sleep(delayBetweenNumbers);
    }


}
