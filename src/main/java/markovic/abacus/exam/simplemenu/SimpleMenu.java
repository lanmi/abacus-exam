package markovic.abacus.exam.simplemenu;

import lombok.extern.slf4j.Slf4j;
import markovic.abacus.exam.ExamExecutor;
import markovic.abacus.exam.NumberGenerator;

@Slf4j
public class SimpleMenu {

    private final int LONG_DELAY = 5000;
    private final int SHORT_DELAY = 3000;
    private final int VERY_SHORT_DELAY = 2000;
    private final int AUDIO_LIMITED_DELAY = 1000;

    private ExamExecutor examExecutor = ExamExecutor.getInstance();

    public void hardSlow5() {
        examExecutor.start(LONG_DELAY, NumberGenerator.generateOneDigitUnsignedNumbers(5));
    }

    public void hardFast5() {
        examExecutor.start(SHORT_DELAY, NumberGenerator.generateOneDigitUnsignedNumbers(10));
    }

    public void hardSlow10() {
        examExecutor.start(LONG_DELAY, NumberGenerator.generateOneDigitUnsignedNumbers(10));
    }

    public void hardFast10() {
        examExecutor.start(SHORT_DELAY, NumberGenerator.generateOneDigitUnsignedNumbers(10));
    }

    public void easySlow5() {
        examExecutor.start(LONG_DELAY, NumberGenerator.generateOneDigitSignedNoAlgorithmNumberList(5));
    }

    public void easyFast5() {
        examExecutor.start(SHORT_DELAY, NumberGenerator.generateOneDigitSignedNoAlgorithmNumberList(5));
    }

    public void easySlow10() {
        examExecutor.start(LONG_DELAY, NumberGenerator.generateOneDigitSignedNoAlgorithmNumberList(5));
    }

    public void easyFast10() {
        examExecutor.start(SHORT_DELAY, NumberGenerator.generateOneDigitSignedNoAlgorithmNumberList(10));
    }

    public void easyVeryFast5() {
        examExecutor.start(VERY_SHORT_DELAY, NumberGenerator.generateOneDigitSignedNoAlgorithmNumberList(5));
    }

    public void easyVeryFast10() {
        examExecutor.start(VERY_SHORT_DELAY, NumberGenerator.generateOneDigitSignedNoAlgorithmNumberList(10));
    }

}