package markovic.abacus.exam;

import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.Objects;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class MainFx extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        System.out.println("Starting");
        Locale.setDefault(Locale.forLanguageTag("sr"));
        String menuFile = System.getProperties().getProperty("menufile", "fxml/AdvancedMenu.fxml");
        System.out.println("Using variable 'menufile' = '" + menuFile + "'");
        URL resource = getClass().getClassLoader().getResource(menuFile);
        Parent root = FXMLLoader.load(Objects.requireNonNull(resource));
        primaryStage.setTitle("Abakus");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    @Override
    public void stop() {
        Platform.exit();
        System.exit(0);
    }
}
