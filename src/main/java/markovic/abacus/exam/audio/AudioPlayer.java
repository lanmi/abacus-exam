package markovic.abacus.exam.audio;

import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import lombok.extern.slf4j.Slf4j;
import markovic.abacus.exam.Translation;

@Slf4j
public class AudioPlayer {

    private final Object o = new Object();

    private Executor executor = Executors.newSingleThreadExecutor();

    public AudioPlayer() {
        super();
    }

    public static void main(String[] args) {
        AudioPlayer ap = new AudioPlayer();
        Locale SERBIAN = Locale.forLanguageTag("sr");
        Locale.setDefault(SERBIAN);

        // play 1 to 9 numbers
        for (int i = 1; i < 10; i++) {
            ap.playNumber(i);
        }
    }

    public void play(String message) {
        String filename = Translation.getString("audio." + message);
        Optional.ofNullable(this.getClass().getResource(filename))
                .ifPresent(this::play);
    }

    public void playNonBlocking(String message) {
        executor.execute(() -> play(message));
    }

    public void playNumber(int number) {
        log.debug("play " + number);
        if (number >= 0) {
            play("p" + Math.abs(number));
        } else {
            play("m" + Math.abs(number));
        }
    }

    public void playNumberNonBlocking(int number) {
        executor.execute(() -> this.playNumber(number));
    }

    protected void play(URL url) throws IllegalStateException {
        log.info("Playing {}", url);
        Clip clip = null;
        try {
            clip = AudioSystem.getClip();
            AudioInputStream ais = AudioSystem.getAudioInputStream(url);
            clip.open(ais);
            clip.start();
            clip.addLineListener(this::notifyOnLineStopped);
            waitTillStop();
        } catch (IOException | UnsupportedAudioFileException | InterruptedException | LineUnavailableException e) {
            throw new IllegalStateException(e);
        } finally {
            Optional.ofNullable(clip)
                    .filter(Clip::isOpen)
                    .ifPresent(Clip::close);
        }

    }

    private void notifyOnLineStopped(LineEvent evt) {
        if (Objects.equals(evt.getType(), LineEvent.Type.STOP)) {
            lineStoped();
        }
    }

    private void lineStoped() {
        synchronized (o) {
            o.notify();
        }
    }

    private void waitTillStop() throws InterruptedException {
        synchronized (o) {
            o.wait();
        }
    }

}
