package markovic.abacus.exam;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

@SuppressWarnings("nls")
public class Translation {

	private Translation() {
		// do not instantiate
	}

	private static String BUNDLE_NAME = "translations.messages"; //$NON-NLS-1$
	private static ResourceBundle resourceBundle;
	private static Locale currentLocale = Locale.getDefault();

	private static ResourceBundle loadBundle() {
		return ResourceBundle.getBundle(BUNDLE_NAME);
	}

	public static String getString(final String key) {
		try {
			return getBundle().getString(key);
		} catch (final MissingResourceException e) {
			return "!" + key + "!";
		}
	}

	private static ResourceBundle getBundle() {
		if (isLocaleChanged() || !isInitialized()) {
			resourceBundle = loadBundle();
			currentLocale = Locale.getDefault();
		}
		return resourceBundle;
	}

	private static boolean isLocaleChanged() {
		return Locale.getDefault() != currentLocale;
	}

	private static boolean isInitialized() {
		return resourceBundle != null;
	}

	public static boolean containsKey(final String key) {
		return resourceBundle.containsKey(key);
	}

	/** Strings array access. The separation is by {@code ,} value. */
	public static String[] getStringArray(final String key) {
		return getString(key).split("\\s*,\\s*"); //$NON-NLS-1$
	}

}
