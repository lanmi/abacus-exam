package markovic.abacus.exam.controller.advancedmenu;

import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Function;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.Spinner;
import lombok.extern.slf4j.Slf4j;
import markovic.abacus.exam.ExamExecutor;
import markovic.abacus.exam.NumberGenerator;
import markovic.abacus.exam.Translation;

@Slf4j
public class AdvancedMenu implements Initializable {

    private ExamExecutor examExecutor = ExamExecutor.getInstance();

    @FXML
    private Slider speedSlider;

    @FXML
    private Label speedValueLabel;

    @FXML
    private Spinner<Integer> repetitionsCounter;

    @FXML
    private ChoiceBox<Complexity> complexityChoice;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        speedValueLabel.textProperty().bind(speedSlider.valueProperty().asString("%.2f na minuto"));
        complexityChoice.setItems(FXCollections.observableArrayList(Complexity.values()));
        complexityChoice.setValue(Complexity.ONE_DIGIT_NO_ALGORITHM_ADD_SUB);
    }

    public void start() {
        int delay = speedToDelay(speedSlider.getValue());
        int repetitions = repetitionsCounter.getValue();
        log.info("Starting exam with {} numbers and delay {} ms", repetitions, delay);
        List<Integer> integerList = complexityChoice.getValue().getNumberGenerator().apply(repetitions);
        examExecutor.start(delay, integerList);
    }

    /**
     * @param speed is in numbers per minute
     */
    private int speedToDelay(double speed) {
        int millisecondsInMinute = 60_000;
        return (int) Math.round(millisecondsInMinute / speed);
    }

}
