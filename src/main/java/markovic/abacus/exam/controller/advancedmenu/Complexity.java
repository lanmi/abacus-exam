package markovic.abacus.exam.controller.advancedmenu;

import java.util.List;
import java.util.function.Function;

import markovic.abacus.exam.NumberGenerator;
import markovic.abacus.exam.Translation;

public enum Complexity {
    ONE_DIGIT_NO_ALGORITHM_ADD_SUB(NumberGenerator::generateOneDigitSignedNoAlgorithmNumberList),
    ONE_DIGIT_5s_ALGORITHM_ADD_SUB(NumberGenerator::generateSigned5sAlgorithmNumberList),
    ONE_DIGIT_ALL_ADD(NumberGenerator::generateOneDigitUnsignedNumbers),
    ONE_DIGIT_ALL_ADD_SUB(NumberGenerator::generateSignedAllAlgorithmsNumberList),
    TWO_DIGITS_NO_ALGORITHM_ADD_SUB(NumberGenerator::generateTwoDigitSignedNoAlgorithmNumber);

    private final Function<Integer, List<Integer>> numberGenerator;

    Complexity(Function<Integer, List<Integer>> numberGenerator) {
        this.numberGenerator = numberGenerator;
    }

    public Function<Integer, List<Integer>> getNumberGenerator() {
        return numberGenerator;
    }

    @Override
    public String toString() {
        String key = this.getDeclaringClass().getSimpleName() + "." + this.name();
        return Translation.getString(key.toLowerCase());
    }
}