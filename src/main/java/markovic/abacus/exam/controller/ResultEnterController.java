package markovic.abacus.exam.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import lombok.Setter;
import markovic.abacus.exam.audio.AudioPlayer;

public class ResultEnterController implements Initializable {

    @FXML
    private Button closeButton;

    @FXML
    private TextField resultTextField;

    @FXML
    private Label yesLabel;

    @FXML
    private Label noLabel;

    @FXML
    private HBox correctResultPanel;

    @FXML
    private Label resultLabel;

    @Setter
    private int expectedResult = 5;

    private boolean isAllowedToCheckResult = false;

    private AudioPlayer audioPlayer = new AudioPlayer();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        resultTextField.textProperty().addListener((value, oldValue, newValue) -> resultTextChanged(newValue));
    }

    public void checkResult() {
        if (!isAllowedToCheckResult) {
            return;
        }
        if (Integer.parseInt(resultTextField.getText()) == expectedResult) {
            yesLabel.setVisible(true);
            noLabel.setVisible(false);
            audioPlayer.playNonBlocking("success");
        } else {
            yesLabel.setVisible(false);
            noLabel.setVisible(true);
            audioPlayer.playNonBlocking("fail");
        }
        resultTextField.setDisable(true);
        resultLabel.setText(Integer.toString(expectedResult));
        correctResultPanel.setVisible(true);
    }

    public void resultTextChanged(String newValue) {
        boolean isAllowed = isParseableInt(newValue);
        if (!isAllowed) {
            disallowResultCheck();
        } else {
            allowResultCheck();
        }
    }

    private boolean isParseableInt(String text) {
        try {
            Integer.parseInt(text);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private void allowResultCheck() {
        isAllowedToCheckResult = true;
        resultTextField.setBackground(new Background(new BackgroundFill(Paint.valueOf("white"), null, null)));
    }

    private void disallowResultCheck() {
        isAllowedToCheckResult = false;
        resultTextField.setBackground(new Background(new BackgroundFill(Paint.valueOf("pink"), null, null)));
    }

    public void close() {
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }
}
