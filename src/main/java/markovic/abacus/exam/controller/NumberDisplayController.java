package markovic.abacus.exam.controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import static markovic.abacus.exam.utilites.Util.sleep;

public class NumberDisplayController {

    @FXML
    public Label number;

    public void setNumber(int value) {
        Platform.runLater(() -> number.setText(""));
        sleep(100);
        Platform.runLater(() -> number.setText(Integer.toString(value)));
    }

    public void close() {
        Platform.runLater(() -> {
            Stage stage = (Stage) number.getScene().getWindow();
            stage.close();
        });
    }

}
