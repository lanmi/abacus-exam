package markovic.abacus.exam.controller;

import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import markovic.abacus.exam.audio.AudioPlayer;

@Slf4j
public class SemaphoreController {

    @FXML
    public Circle redLight;
    @FXML
    public Circle redLightOff;
    @FXML
    public Circle yellowLight;
    @FXML
    public Circle yellowLightOff;
    @FXML
    public Circle greenLight;
    @FXML
    public Circle greenLightOff;

    private AudioPlayer audioPlayer = new AudioPlayer();

    @Setter
    private long tickInMillis = 1000;

    @Setter
    private Runnable callback;

    private static final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor(r -> {
        Thread t = new Thread(r);
        t.setName("semaphore-tick");
        return t;
    });


    public void play() {
        scheduler.schedule(this::lightRed, 0, TimeUnit.MILLISECONDS);
        scheduler.schedule(this::lightYellowAfterRed, tickInMillis, TimeUnit.MILLISECONDS);
        scheduler.schedule(this::lightGreen, 2 * tickInMillis, TimeUnit.MILLISECONDS);
        scheduler.schedule(() -> Platform.runLater(this::close), 2 * tickInMillis + tickInMillis / 2,
                TimeUnit.MILLISECONDS);
    }

    private void lightRed() {
        redLight.setVisible(true);
        redLightOff.setVisible(false);
        yellowLight.setVisible(false);
        yellowLightOff.setVisible(true);
        greenLight.setVisible(false);
        greenLightOff.setVisible(true);
        audioPlayer.playNonBlocking("semaphore.red");
    }

    private void lightYellowAfterRed() {
        redLight.setVisible(true);
        redLightOff.setVisible(false);
        yellowLight.setVisible(true);
        yellowLightOff.setVisible(false);
        greenLight.setVisible(false);
        greenLightOff.setVisible(true);
        audioPlayer.playNonBlocking("semaphore.yellow");
    }

    private void lightGreen() {
        redLight.setVisible(false);
        redLightOff.setVisible(true);
        yellowLight.setVisible(false);
        yellowLightOff.setVisible(true);
        greenLight.setVisible(true);
        greenLightOff.setVisible(false);
        audioPlayer.playNonBlocking("semaphore.green");
    }

    private void close() {
        Stage stage = (Stage) redLight.getScene().getWindow();
        stage.close();
        Optional.ofNullable(callback).ifPresent(Runnable::run);
    }

}
