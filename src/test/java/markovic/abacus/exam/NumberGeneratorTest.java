package markovic.abacus.exam;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NumberGeneratorTest {

	private static final Logger log = LoggerFactory.getLogger(NumberGeneratorTest.class);

	@Test
	public void testOneDigitSinedNumberGenerator() {
		List<Integer> list = NumberGenerator.generateOneDigitSignedNumbers(1000);
		assertEquals(1000, list.size());
		for (int num : list) {
			assertTrue("Unexpected number found: (" + num + ")", num >= -9);
			assertTrue("Unexpected number found: (" + num + ")", num <= 9);
			assertTrue("Unexpected number found: (" + num + ")", num != 0);
			log.info("Number {}", num);
			System.out.println("number " + num);
		}
	}

	@Test
	public void testOneDigitUnsigneddNumberGenerator() {
		List<Integer> list = NumberGenerator.generateOneDigitUnsignedNumbers(1000);
		assertEquals(1000, list.size());
		for (int num : list) {
			assertTrue("Unexpected number found: (" + num + ")", num >= 1);
			assertTrue("Unexpected number found: (" + num + ")", num <= 9);
			log.info("Number {}", num);
			System.out.println("number " + num);
		}
	}

	@Test
	public void testOneDigitNoAlgorighTest() {
		List<Integer> list = NumberGenerator.generateOneDigitSignedNoAlgorithmNumberList(1000);
		assertEquals(1000, list.size());
		int sum = 0;
		for (int num : list) {
			assertTrue("Unexpected number found: (" + num + ")", num >= -9);
			assertTrue("Unexpected number found: (" + num + ")", num <= 9);
			assertTrue("Unexpected number found: (" + num + ")", num != 0);
			log.info("Number {}", num);
			System.out.println("number " + num);
			sum += num;
			assertTrue("Invalid sum: (" + sum + ")", sum >= 0);
		}
	}

	@Test
	public void testGetDigit() {
		int number = 123456;
		assertEquals(6,NumberGenerator.getDigit(number,0));
		assertEquals(5,NumberGenerator.getDigit(number,1));
		assertEquals(4,NumberGenerator.getDigit(number,2));
		assertEquals(3,NumberGenerator.getDigit(number,3));
		assertEquals(2,NumberGenerator.getDigit(number,4));
		assertEquals(1,NumberGenerator.getDigit(number,5));
	}

	@Test
	public void testGetDigit_NumberZero() {
		int number = 0;
		assertEquals(0,NumberGenerator.getDigit(number,0));
		assertEquals(0,NumberGenerator.getDigit(number,1));
		assertEquals(0,NumberGenerator.getDigit(number,2));
	}
}
